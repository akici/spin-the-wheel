(function(){
 'use strict';
  var el = document.querySelector('.odometer');
  var od = new Odometer({
    el: el,
    value: 10000,

    // Any option (other than auto and selector) can be passed in here
    format: 'dddd',
    theme: 'car'
  });

  var count= 10000;

  function counter(){
    count++;
    if(count>19999){
      el.innerHTML = '1BOOM';
    }
    else {
      setTimeout(function () {
        el.innerHTML = count;
      }, 100);
    }
    var clear = document.getElementById('clear');
    clear.onclick = function(){
      count = 10000;
      od.update(10000);
    };
  }

  if ('onmousewheel' in document) {
    document.onmousewheel = counter;
  }
  else {
    document.addEventListener('DOMMouseScroll', counter, false);
  }


}());
